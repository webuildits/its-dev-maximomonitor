"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var menu_service_1 = require("./menu.service");
var AppComponent = (function () {
    function AppComponent(menuService, fb) {
        this.menuService = menuService;
        this.fb = fb;
        this.loginForm = this.fb.group({
            login: ["", forms_1.Validators.required],
            password: ["", forms_1.Validators.required]
        });
        this.authenticated = true;
    }
    AppComponent.prototype.getMenuItem = function () {
        var _this = this;
        this.menuService.getMenuItem().then(function (menu) { return _this.menu = menu; });
    };
    AppComponent.prototype.ngOnInit = function () {
        this.currentPage = { 'title': "Login", 'link': "login" };
        this.currentPage = { 'title': "Overview", 'link': "overview" };
        this.getMenuItem();
    };
    AppComponent.prototype.onSelect = function (link) {
        this.currentPage = link;
    };
    AppComponent.prototype.doLogin = function () {
        if (this.loginForm.value.login == "admin" && this.loginForm.value.password == "password") {
            this.authenticated = true;
            this.onSelect(this.menu[0]);
        }
    };
    AppComponent.prototype.doLogout = function () {
        this.authenticated = false;
        console.log("logout");
        this.onSelect({ 'title': "Login", 'link': "login" });
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'mApp',
        template: "\n  <div id=\"top\">\n    <div>\n      <span class=\"appTitle\">MaxAware - Built by ITS</span>\n      <span *ngIf=\"authenticated==true\" class=\"logout\" (click)=\"doLogout($event)\">Log Out</span>\n    </div>\n  </div>\n  <div id=\"menu\">\n    <div class=\"menu\" *ngIf=\"authenticated==true\">\n      <div *ngFor=\"let i of menu\">\n        <a [class.selected]=\"i === currentPage\" (click)=\"onSelect(i)\">{{i.title}}</a>\n      </div>\n    </div>\n    <div class=\"menu\" *ngIf=\"authenticated!=true\">\n      <div>\n        <a [class.selected]=\"true\">Login</a>\n      </div>\n    </div>\n  </div>\n  <div id=\"main\">\n    <div>\n      <mContent [currentPage]=\"currentPage\"></mContent>\n      <span *ngIf=\"currentPage.link==='login'\" class=\"login\">\n        <form [formGroup]=\"loginForm\" name=\"loginForm\" (ngSubmit)=\"doLogin($event)\">\n          <div><label for=\"login\">Login: </label><input formControlName=\"login\" type=\"email\"></div>\n          <div><label for=\"password\">Password: </label><input formControlName=\"password\" type=\"password\"></div>\n          <div><input id=\"loginButton\" value=\"Login\" type=\"submit\" class=\"button\"></div>\n        </form>\n      </span>\n    </div>\n  </div>\n  ",
        providers: [menu_service_1.MenuService]
    }),
    __metadata("design:paramtypes", [menu_service_1.MenuService, forms_1.FormBuilder])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map
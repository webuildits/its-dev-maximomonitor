import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators }   from '@angular/forms';

import { Menu } from './menu';
import { MenuService } from './menu.service';
import { Instance } from './instance';

@Component({
  selector: 'mApp',
  template: `
  <div id="top">
    <div>
      <span class="appTitle">MaxAware - Built by ITS</span>
      <span *ngIf="authenticated==true" class="logout" (click)="doLogout($event)">Log Out</span>
    </div>
  </div>
  <div id="menu">
    <div class="menu" *ngIf="authenticated==true">
      <div *ngFor="let i of menu">
        <a [class.selected]="i === currentPage" (click)="onSelect(i)">{{i.title}}</a>
      </div>
    </div>
    <div class="menu" *ngIf="authenticated!=true">
      <div>
        <a [class.selected]="true">Login</a>
      </div>
    </div>
  </div>
  <div id="main">
    <div>
      <mContent [currentPage]="currentPage"></mContent>
      <span *ngIf="currentPage.link==='login'" class="login">
        <form [formGroup]="loginForm" name="loginForm" (ngSubmit)="doLogin($event)">
          <div><label for="login">Login: </label><input formControlName="login" type="email"></div>
          <div><label for="password">Password: </label><input formControlName="password" type="password"></div>
          <div><input id="loginButton" value="Login" type="submit" class="button"></div>
        </form>
      </span>
    </div>
  </div>
  `,
  providers: [MenuService]
})

export class AppComponent implements OnInit {
  menu: Menu[];
  currentPage: Menu;
  
  public loginForm = this.fb.group({
    login: ["", Validators.required],
    password: ["", Validators.required]
  });
    
  constructor(private menuService: MenuService, public fb: FormBuilder) { }
  
  authenticated = true;

  getMenuItem(): void {
    this.menuService.getMenuItem().then(menu => this.menu = menu);
  }
  
  ngOnInit(): void {
    this.currentPage = {'title': "Login", 'link': "login"};
    this.currentPage = {'title': "Overview", 'link': "overview"};
    this.getMenuItem();
  }

  onSelect(link: Menu): void {
    this.currentPage = link;
  }
  
  doLogin(): void {
    if (this.loginForm.value.login == "admin" && this.loginForm.value.password == "password") {
      this.authenticated = true;
      this.onSelect(this.menu[0]);
    }
  }
  
  doLogout(): void {
    this.authenticated = false;
    console.log("logout");
    this.onSelect({'title': "Login", 'link': "login"});
  }
}
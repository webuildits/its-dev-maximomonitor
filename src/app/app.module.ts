import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { ReactiveFormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { ContentComponent }  from './content.component';
import { InstanceService } from './instance.service';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule 
   ],
  declarations: [
    AppComponent,
    ContentComponent
  ],
  providers: [
    InstanceService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
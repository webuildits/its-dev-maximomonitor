"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
//import { RouterModule, Routes } from '@angular/router';
//import { ActivatedRoute } from '@angular/router';
var common_1 = require("@angular/common");
var menu_1 = require("./menu");
var instance_service_1 = require("./instance.service");
/*
const routes: Routes = [
  { path: '', redirectTo: '/overview', pathMatch: 'full' },
  { path: 'overview/:id', component: ContentComponent }
];
*/
var ContentComponent = (function () {
    function ContentComponent(instanceService, location) {
        this.instanceService = instanceService;
        this.location = location;
    }
    ContentComponent.prototype.ngOnInit = function () {
        // NOT the best way to do this...?
        this.instanceID = this.location.path().substring(1).split("/")[0];
        this.getInstances();
    };
    ContentComponent.prototype.getInstances = function () {
        var _this = this;
        this.instanceService
            .getInstances()
            .then(function (instance) { return _this.instance = instance; });
    };
    return ContentComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", menu_1.Menu)
], ContentComponent.prototype, "currentPage", void 0);
ContentComponent = __decorate([
    core_1.Component({
        selector: 'mContent',
        template: "\n  <span *ngIf=\"currentPage\">\n    <h1 class=\"title\">{{ currentPage.title }}</h1>\n\n    <div id=\"content\">\n      <div *ngIf=\"currentPage.link==='overview'\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <h3 class=\"subtitle\">{{ i.instancename }}<span>(v{{ i.maxVersion }})</span></h3>\n            <div class=\"instanceUrl\"><a href=\"{{ i.maxURL }}\">{{ i.maxURL }}</a></div>\n            <h4 class=\"subtitle2\">Servers:</h4>\n\n            <div *ngIf=\"i.serverArray.length > 0\">\n              <div *ngFor=\"let s of i.serverArray\">\n                <h5>{{ s.serverName }}</h5>\n                <div class=\"indentLeft\">\n                  Role(s):\n                  <span *ngIf=\"s.isApp > 0\">Application</span>\n                  <span *ngIf=\"(s.isApp+s.isDB) > 1\">, </span>\n                  <span *ngIf=\"s.isDB > 0\">Database</span>\n                </div>\n                <div class=\"indentLeft\">\n                  Status: No issues detected.\n                </div>\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div>\n\n      <div *ngIf=\"currentPage.link==='diskspace'\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <div *ngFor=\"let s of i.serverArray\">\n              <h3 class=\"subtitle\">{{ s.serverName }}</h3>\n              <div *ngFor=\"let x of s.hardware\">\n                <div *ngIf=\"x.type==='HDD'\">\n                  <div class=\"storageGaugeContainer\">\n                    {{ x.currentLevel | number:'1.1-1' }}{{ x.uom }} of {{ x.capacity | number:'1.1-1' }}{{ x.uom }} used ({{ (1 - ((x.currentLevel)/(x.capacity))) | percent:'1.0-0' }} free)\n                    <div>\n                      <div *ngIf=\"((x.currentLevel)/(x.capacity)*100)<80\" [style.width]=\"((x.currentLevel)/(x.capacity)) | percent:'1.0-0'\">&nbsp;</div>\n                      <div *ngIf=\"((x.currentLevel)/(x.capacity)*100)>=80 && ((x.currentLevel)/(x.capacity)*100)<95\" [style.width]=\"((x.currentLevel)/(x.capacity)) | percent:'1.0-0'\" class=\"warn\">&nbsp;</div>\n                      <div *ngIf=\"((x.currentLevel)/(x.capacity)*100)>=95\" [style.width]=\"((x.currentLevel)/(x.capacity)) | percent:'1.0-0'\" class=\"critical\">&nbsp;</div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div *ngIf=\"currentPage.link==='adminmode'\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <span *ngIf=\"i.adminMode===true\">\n              <label class=\"adminModeSwitch\"><input type=\"checkbox\" checked=\"true\"><span class=\"slider\"></span></label>\n              <h3>Admin Mode is currently on for this instance.</h3>\n            </span>\n            <span *ngIf=\"i.adminMode!==true\">\n              <label class=\"adminModeSwitch\"><input type=\"checkbox\"><span class=\"slider\"></span></label>\n              <h3>Admin Mode is currently off for this instance.</h3>\n            </span>\n          </div>\n        </div>\n      </div>\n      \n      <div *ngIf=\"currentPage.link==='licensing'\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <div *ngFor=\"let l of i.licensing\">\n              <div class=\"licensingGaugeContainer\" *ngIf=\"((l.used)/(l.allowed)*100) < 100 && l.level!=='Self Service'\">\n                {{ l.used | number:'1.0-0' }} of {{ l.allowed | number:'1.0-0' }} {{ l.level }} licenses used ({{ ((l.allowed)-(l.used)) }} available)\n                <div>\n                  <div [style.width]=\"((l.used)/(l.allowed)) | percent:'1.0-0'\">&nbsp;</div>\n                </div>\n              </div>\n              <div class=\"licensingGaugeContainer\" *ngIf=\"((l.used)/(l.allowed)*100) >= 100 && l.level!=='Self Service'\">\n                {{ l.used | number:'1.0-0' }} of {{ l.allowed | number:'1.0-0' }} {{ l.level }} licenses used ({{ ((l.used)-(l.allowed)) }} over licensed amount)\n                <div>\n                  <div class=\"licensingViolation\">&nbsp;</div>\n                </div>\n              </div>\n              <div class=\"licensingGaugeContainer\" *ngIf=\"l.level==='Self Service'\">\n                {{ l.used | number:'1.0-0' }} of Unlimited {{ l.level }} licenses used\n                <div>\n                  <div>&nbsp;</div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      \n      <div *ngIf=\"currentPage.link==='sqlconnection'\" class=\"sqlConnection\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <div class=\"circle\"><div></div></div>\n            <div><h3>SQL database connection for {{ i.instancename }} is UP.</h3></div>\n          </div>\n        </div>\n      </div>\n      \n      <div *ngIf=\"currentPage.link==='systemup'\" class=\"system\">\n        <div *ngFor=\"let i of instance\">\n          <div *ngIf=\"i.instanceUID===instanceID\">\n            <div class=\"circle\"><div></div></div>\n            <div><h3>{{ i.instancename }} Maximo instance is UP.</h3></div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n  </span>\n  ",
        providers: [common_1.Location, { provide: common_1.LocationStrategy, useClass: common_1.PathLocationStrategy }, instance_service_1.InstanceService]
    }),
    __metadata("design:paramtypes", [instance_service_1.InstanceService,
        common_1.Location])
], ContentComponent);
exports.ContentComponent = ContentComponent;
//# sourceMappingURL=content.component.js.map
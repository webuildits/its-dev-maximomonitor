import { Component, OnInit, Input } from '@angular/core';
//import { RouterModule, Routes } from '@angular/router';
//import { ActivatedRoute } from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

import { Menu } from './menu';
import { Instance } from './instance';
import { InstanceService } from './instance.service';

/*
const routes: Routes = [
  { path: '', redirectTo: '/overview', pathMatch: 'full' },
  { path: 'overview/:id', component: ContentComponent }
];
*/

@Component({
  selector: 'mContent',
  template: `
  <span *ngIf="currentPage">
    <h1 class="title">{{ currentPage.title }}</h1>

    <div id="content">
      <div *ngIf="currentPage.link==='overview'">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <h3 class="subtitle">{{ i.instancename }}<span>(v{{ i.maxVersion }})</span></h3>
            <div class="instanceUrl"><a href="{{ i.maxURL }}">{{ i.maxURL }}</a></div>
            <h4 class="subtitle2">Servers:</h4>

            <div *ngIf="i.serverArray.length > 0">
              <div *ngFor="let s of i.serverArray">
                <h5>{{ s.serverName }}</h5>
                <div class="indentLeft">
                  Role(s):
                  <span *ngIf="s.isApp > 0">Application</span>
                  <span *ngIf="(s.isApp+s.isDB) > 1">, </span>
                  <span *ngIf="s.isDB > 0">Database</span>
                </div>
                <div class="indentLeft">
                  Status: No issues detected.
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div *ngIf="currentPage.link==='diskspace'">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <div *ngFor="let s of i.serverArray">
              <h3 class="subtitle">{{ s.serverName }}</h3>
              <div *ngFor="let x of s.hardware">
                <div *ngIf="x.type==='HDD'">
                  <div class="storageGaugeContainer">
                    {{ x.currentLevel | number:'1.1-1' }}{{ x.uom }} of {{ x.capacity | number:'1.1-1' }}{{ x.uom }} used ({{ (1 - ((x.currentLevel)/(x.capacity))) | percent:'1.0-0' }} free)
                    <div>
                      <div *ngIf="((x.currentLevel)/(x.capacity)*100)<80" [style.width]="((x.currentLevel)/(x.capacity)) | percent:'1.0-0'">&nbsp;</div>
                      <div *ngIf="((x.currentLevel)/(x.capacity)*100)>=80 && ((x.currentLevel)/(x.capacity)*100)<95" [style.width]="((x.currentLevel)/(x.capacity)) | percent:'1.0-0'" class="warn">&nbsp;</div>
                      <div *ngIf="((x.currentLevel)/(x.capacity)*100)>=95" [style.width]="((x.currentLevel)/(x.capacity)) | percent:'1.0-0'" class="critical">&nbsp;</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div *ngIf="currentPage.link==='adminmode'">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <span *ngIf="i.adminMode===true">
              <label class="adminModeSwitch"><input type="checkbox" checked="true"><span class="slider"></span></label>
              <h3>Admin Mode is currently on for this instance.</h3>
            </span>
            <span *ngIf="i.adminMode!==true">
              <label class="adminModeSwitch"><input type="checkbox"><span class="slider"></span></label>
              <h3>Admin Mode is currently off for this instance.</h3>
            </span>
          </div>
        </div>
      </div>
      
      <div *ngIf="currentPage.link==='licensing'">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <div *ngFor="let l of i.licensing">
              <div class="licensingGaugeContainer" *ngIf="((l.used)/(l.allowed)*100) < 100 && l.level!=='Self Service'">
                {{ l.used | number:'1.0-0' }} of {{ l.allowed | number:'1.0-0' }} {{ l.level }} licenses used ({{ ((l.allowed)-(l.used)) }} available)
                <div>
                  <div [style.width]="((l.used)/(l.allowed)) | percent:'1.0-0'">&nbsp;</div>
                </div>
              </div>
              <div class="licensingGaugeContainer" *ngIf="((l.used)/(l.allowed)*100) >= 100 && l.level!=='Self Service'">
                {{ l.used | number:'1.0-0' }} of {{ l.allowed | number:'1.0-0' }} {{ l.level }} licenses used ({{ ((l.used)-(l.allowed)) }} over licensed amount)
                <div>
                  <div class="licensingViolation">&nbsp;</div>
                </div>
              </div>
              <div class="licensingGaugeContainer" *ngIf="l.level==='Self Service'">
                {{ l.used | number:'1.0-0' }} of Unlimited {{ l.level }} licenses used
                <div>
                  <div>&nbsp;</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div *ngIf="currentPage.link==='sqlconnection'" class="sqlConnection">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <div class="circle"><div></div></div>
            <div><h3>SQL database connection for {{ i.instancename }} is UP.</h3></div>
          </div>
        </div>
      </div>
      
      <div *ngIf="currentPage.link==='systemup'" class="system">
        <div *ngFor="let i of instance">
          <div *ngIf="i.instanceUID===instanceID">
            <div class="circle"><div></div></div>
            <div><h3>{{ i.instancename }} Maximo instance is UP.</h3></div>
          </div>
        </div>
      </div>

    </div>
  </span>
  `,
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}, InstanceService]
})

export class ContentComponent  {
  @Input() currentPage: Menu;

  instance: Instance[];
  currentInstance: Instance;
  instanceID: string;

  constructor(
      private instanceService: InstanceService,
      private location: Location
    ) { }
    
  ngOnInit(): void {
// NOT the best way to do this...?
    this.instanceID = this.location.path().substring(1).split("/")[0];
    this.getInstances();
  }
  
  getInstances(): void {
    this.instanceService
      .getInstances()
      .then(instance => this.instance = instance);
  }
  
  /*
  getInstance(): void {
    this.instanceService
      .getInstances()
      .subscribe(currentInstance => this.currentInstance = instance);
  }
  */

}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var InstanceService = (function () {
    function InstanceService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.instanceUrl = 'http://192.168.1.102:5500/mx-data'; // URL to web api
    }
    InstanceService.prototype.getInstances = function () {
        return this.http.get(this.instanceUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    InstanceService.prototype.getInstance = function (id) {
        var url = this.instanceUrl + "/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    /*
    create(name: string): Promise<Instance> {
      return this.http
        .post(this.instanceUrl, JSON.stringify({name: name}), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data as Instance)
        .catch(this.handleError);
    }
  
    update(hero: Instance): Promise<Instance> {
      const url = `${this.instanceUrl}/${instance.id}`;
      return this.http
        .put(url, JSON.stringify(instance), {headers: this.headers})
        .toPromise()
        .then(() => instance)
        .catch(this.handleError);
    }
    */
    InstanceService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return InstanceService;
}());
InstanceService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], InstanceService);
exports.InstanceService = InstanceService;
//# sourceMappingURL=instance.service.js.map
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Instance } from './instance';

@Injectable()
export class InstanceService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private instanceUrl = 'http://192.168.1.102:5500/mx-data';  // URL to web api

  constructor(private http: Http) { }

  getInstances(): Promise<Instance[]> {
    return this.http.get(this.instanceUrl)
      .toPromise()
      .then(
          response => response.json() as Instance[]
        )
      .catch(this.handleError);
  }

  getInstance(id: string): Promise<Instance> {
    const url = `${this.instanceUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Instance)
      .catch(this.handleError);
  }

  /*
  create(name: string): Promise<Instance> {
    return this.http
      .post(this.instanceUrl, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as Instance)
      .catch(this.handleError);
  }

  update(hero: Instance): Promise<Instance> {
    const url = `${this.instanceUrl}/${instance.id}`;
    return this.http
      .put(url, JSON.stringify(instance), {headers: this.headers})
      .toPromise()
      .then(() => instance)
      .catch(this.handleError);
  }
  */

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  /*
  getInstance(): Promise<Instance[]> {
    return Promise.resolve(INSTANCE);
  }
  */
}
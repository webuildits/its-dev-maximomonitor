export class Instance {
    id: string;
    instanceName: string;
    instanceUID: string;
    maxURL: string;
    maxVersion: string;
    adminMode: boolean;
    serverArray: Array<Object>;
}
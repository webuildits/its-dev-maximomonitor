import { Instance } from './instance';

export const INSTANCE: Instance[] = [
    {
        id: '45563910cf8ca836'
        , instanceName: 'ITS Dev'
        , instanceUID: '123456789ABC'
        , maxURL: 'http://192.168.1.104/maximo/'
        , maxVersion: '7.6.0.5'
        , adminMode: false
        , serverArray: [
            {
                "id": 1,
                "serverName": "VMAXIMO.ITS.LOCAL",
                "isDB": 1,
                "isApp": 1,
                "isUp": 1,
                "lastReading": "2017-10-27T09:47:00-06:00",
                "software": [
                    {
                        "id": 1,
                        "type": "OS",
                        "vendor": "Microsoft",
                        "version": "6.3"
                    },
                    {
                        "id": 2,
                        "type": "DB",
                        "vendor": "Microsoft",
                        "version": "11.00.5343"
                    },
                    {
                        "id": 3,
                        "type": "APP",
                        "vendor": "IBM",
                        "version": "8.5.5.3"
                    }
                ],
                "hardware": [
                    {
                        "id": 1,
                        "type": "CPU",
                        "uom": "GHz",
                        "capacity": "3",
                        "currentLevel": "1.2",
                        "previousLevel": "1",
                        "lastReading": "2017-10-27T09:47:00-06:00",
                        "previousReading": "2017-10-25T09:32:00-06:00"
                    },
                    {
                        "id": 2,
                        "type": "RAM",
                        "uom": "GB",
                        "capacity": "16",
                        "currentLevel": "9",
                        "previousLevel": "10",
                        "lastReading": "2017-10-27T09:47:00-06:00",
                        "previousReading": "2017-10-25T09:32:00-06:00"
                    },
                    {
                        "id": 3,
                        "type": "HDD",
                        "uom": "TB",
                        "capacity": "1",
                        "currentLevel": "0.625",
                        "previousLevel": "0.6",
                        "lastReading": "2017-10-27T09:47:00-06:00",
                        "previousReading": "2017-10-25T09:32:00-06:00"
                    },
                    {
                        "id": 4,
                        "type": "HDD",
                        "uom": "TB",
                        "capacity": "2",
                        "currentLevel": "1.275",
                        "previousLevel": "1.2",
                        "lastReading": "2017-10-27T09:47:00-06:00",
                        "previousReading": "2017-10-25T09:32:00-06:00"
                    }
                ]
            }
        ]
    }
];
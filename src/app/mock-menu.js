"use strict";
exports.MENU = [
    {
        title: 'Overview',
        link: 'overview'
    },
    {
        title: 'Disk Space',
        link: 'diskspace'
    },
    {
        title: 'Admin Mode',
        link: 'adminmode'
    },
    {
        title: 'Licensing',
        link: 'licensing'
    },
    {
        title: 'SQL Connection',
        link: 'sqlconnection'
    },
    {
        title: 'System Up',
        link: 'systemup'
    }
];
//# sourceMappingURL=mock-menu.js.map
import { Menu } from './menu';

export const MENU: Menu[] = [
    {
        title: 'Overview'
        , link: 'overview'
    },
    {
        title: 'Disk Space'
        , link: 'diskspace'
    },
    {
        title: 'Admin Mode'
        , link: 'adminmode'
    },
    {
        title: 'Licensing'
        , link: 'licensing'
    },
    {
        title: 'SQL Connection'
        , link: 'sqlconnection'
    },
    {
        title: 'System Up'
        , link: 'systemup'
    }
];